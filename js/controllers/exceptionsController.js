var app = angular.module('FeedbackModule', []);

app.controller('FeedbackCtrl', function($scope) {


	$scope.feedback =  {
												11001: {
													message1: "Can't apply changes. Shipping with id: ", 
													message2: " already exists.",
													kind: "Error.",
													info: "Duplicated Key.",
													output: function(id){ console.log(this.message1 + id + this.message2) }
												}
											}



  $scope.handleError = function(err, shippingId){
  	var feed = $scope.feedback[err.code]
  	console.log(feed.output(shippingId));

  }

});