'use strict'

var app = angular.module('LoginModule', ['electangular']);

app.controller('LoginCtrl', ['$scope', 'electron', function($scope, electron) {


	const BrowserWindow = electron.BrowserWindow
	const GlobalShortcut = electron.globalShortcut
	let win
	const url = require('url')
	const path = require('path')

	$scope.init = function() {
		createMainWindow()
	}


	function createMainWindow() {
		win = new BrowserWindow({width: 1124, height: 600})

	  win.loadURL(url.format({
	    pathname: path.join(__dirname, 'index.html'),
	    protocol: 'file:',
	    slashes: true
	  }))

	  win.maximize()

	  win.webContents.openDevTools()

	  // Emitted when the window is closed.
	  win.on('closed', () => {
	    // Dereference the window object, usually you would store windows
	    // in an array if your app supports multi windows, this is the time
	    // when you should delete the corresponding element.
	    win = null
	  })

	}
	
	createMainWindow()

}]);