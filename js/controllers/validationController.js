var app = angular.module('ValidationModule', ['ngRoute', 'ngMessages', 'InputModule']);

app.$inject = ['$scope', 'ValidationService', 'InputService', 'ShippingModel', 'ContentModel','FeedbackService'];


app.factory("ValidationService", function(ShippingModel, ContentModel, FeedbackService){

  var self = this

  /* Static cap references
   */
  const limit = { 
                  weight: 50,
                  fob: 1000
                }  

  /* References to validation functions
   */
  const validations = [
                        { name: 'prefix',      func: function(){ return isValidPrefix } },
                        { name: 'id',          func: function(){ return isValidId } },
                        { name: 'packages',    func: function(){ return isValidPackages } },
                        { name: 'weight',      func: function(){ return isValidWeight } },
                        { name: 'currency',    func: function(){ return isValidCurrency } },
                        { name: 'fobAmount',   func: function(){ return isValidFobAmount } },
                        { name: 'consignee',   func: function(){ return isValidConsignee } },
                        { name: 'shipper',     func: function(){ return isValidShipper } },
                        { name: 'measure',     func: function(){ return true } },
                        { name: 'quantity',    func: function(){ return true } },
                        { name: 'description', func: function(){ return true } },
                        { name: 'contentFob',  func: function(){ return true } },
                        { name: 'nadi',        func: function(){ return true } }
                      ];

  var fieldValidations = {
    validIndex:         function(value){ return isValidIndex(value) },
    invalidIndex:       function(value){ return !isValidIndex(value) },
    validPackages:      function(value){ return isValidPackages(value) }, 
    invalidPackages:    function(value){ return !isValidPackages(value) }, 
    validWeight:        function(value){ return isValidWeight(value) }, 
    invalidWeight:      function(value){ return !isValidWeight(value) }, 
    validCurrency:      function(value){ return isValidCurrency(value) },
    invalidCurrency:    function(value){ return !isValidCurrency(value) },
    validFobAmount:     function(value){ return isValidFobAmount(value) },
    invalidFobAmount:   function(value){ return !isValidFobAmount(value) },    
    validFob:           function(value){ return isValidFob(value) },
    invalidFob:         function(value){ return !isValidFob(value) },
    validConsignee:     function(value){ return isValidConsignee(value) },
    invalidConsignee:   function(value){ return !isValidConsignee(value) },
    validShipper:       function(value){ return isValidShipper(value) },
    invalidShipper:     function(value){ return !isValidShipper(value) },
    validQuantity:      function(value){ return isValidQuantity(value) },
    invalidQuantity:    function(value){ return !isValidQuantity(value) }, 
    validDescription:   function(value){ return isValidDescription(value) },
    invalidDescription: function(value){ return !isValidDescription(value) },
    validContentFob:    function(value){ return isValidContentFob(value) },
    invalidContentFob:  function(value){ return !isValidContentFob(value) }
  }                      

  function getShippingPrefixes() {
    return self.prefixes
  }

  /* Purp.: Indicates an input change in a scope variable
   *  where 
   *        value === { data['somePropertyValue'] -> String || Integer || Double }
   */
  function hasChanged(value){
    return value !== ShippingModel.getValue();
  }

  /* Purp.: Indicates an input change in a scope variable
   *  where 
   *        value === { data['somePropertyValue'] -> String || Integer || Double }
   */
  function hasChangedContent(value){
    return value !== ContentModel.getValue();
  }

  /* Purp.: Given a property name returns a validation function
   * Prec.: property must match only one validation name
   *  where 
   *        property === { 'validation.name' -> String}
   */
  function getFunctionByProperty(property) {
    console.log(property)
    var func = validations.filter(function(a) {
      return a.name === property
    });
    return func[0].func
  }

  /* Purp.: Determines when an index (prefix+id) is valid
   *
   *  where 
   *        prefix === { 'data.prefix' -> String }
   *        id     === { 'data.id'     -> Integer }
   */
  function validateIndex(prefix, id) {
    if(prefix === null) {
      console.log("Field 'prefix' can't be empty")
      return false
    }
    if(id === null) {
      console.log("Field 'id' can't be empty")
      return false
    }                                                                           // removed return, currently letting it save the value
    matchesPrefixLength(prefix, id); //removed return from here
    return true
  }

  /* Purp.: similar to matchesPrefixLength()
   *  Used for UI feedback on index fields
   */
  function isValidIndex(data) {
    return matchesPrefixLength(data.prefix, data.id)
  }

  /* Purp.: Evaluates if a prefix matches and id length
   * Prec.: 'prefix' must exist in prefixes
   *
   *  where 
   *        prefix === { 'data.prefix' -> String }
   *        id     === { 'data.id'     -> Integer }
   */
  function matchesPrefixLength(prefix, id) {
    var res = getShippingPrefixes().filter(function(a){
      return a.name === prefix;
    });                                                                           // Currently letting it save the value
    if(res[0].length !== id.length) {                                             // giving UI feedback
      console.log(prefix + " shipping needs " + res[0].length + " digits.")       // Coming up adding a save/dontSave state
      return false
    }
    else {
      return true
    }
  }

  /* Purp.: Determines when a prefix is valid, compared to an id in the scope
   *
   *  where value === { 'data.prefix' -> String }
   */
  var isValidPrefix = function(value) {
    var id = ShippingModel.getIdNumber()
    return validateIndex(value, id)
  }

  /* Purp.: Determines when an id is valid, compared to a prefix in the scope
   *
   *  where 
   *        value === { 'data.id' -> Integer }
   */
  var isValidId = function(value) {
    var prefix = ShippingModel.getPrefix()
    return validateIndex(prefix, value)
  }

  /* Purp.: Determines when a packages value is valid
   *
   *  where 
   *        value === { 'data.packages' -> Integer }
   */
  var isValidPackages = function(value) {
    if(value === '') return false
    return true
  }

  /* Purp.: Determines when a weight value is valid
   *
   *  where 
   *        value === { 'data.weight' -> Integer }
   */
  var isValidWeight = function(value) {
    if(value === '') return false
    var cap = limit.weight
    if(value > cap) {
      console.log("Weight can't exceed " + cap + " kilograms.")
      return false
    }
    if(value === '0') {
      console.log("Weight can't be " + value + ".")
      return false
    }
    return value <= cap
  }

  /* Purp.: Determines when a currency is valid
   *
   *  where
   *        value === { 'data.currency' -> String }
   */
  var isValidCurrency = function(value) {
    if(value === '') return false
    else return true
  }

  /* Purp.: Determines when a fob value is valid
   *
   * where 
   *        value === { 'data.fob' -> Double }
   */
  var isValidFob = function(value) {
    if(value === '') return false
    var cap = limit.fob
    if(value > cap) {
      console.log("Fob can't exceed " + cap + ".")
      return false
    }
    if(value === '0') {
      console.log("Fob can't be " + value + ".")
      return false
    }
    return value <= cap
  }

  /* Purp.: Determines when a fob amount is valid
   *
   *  where
   *        value === { 'data.fob' -> Double }
   */
  var isValidFobAmount = function(value) {
    if(value === '') return false
    else return true
  }

  /* Purp.: Determines when a consignee is valid
   *  where 
   *        value === { 'data.consignee' -> String }
   */
  var isValidConsignee = function(value) {
    if(value === '') return false
    return true
  }

  /* Purp.: Determines when a shipper is valid
   *  where 
   *        value === { 'data.shipper' -> String }
   */
  var isValidShipper = function(value) {
    if(value === '') return false
    return true
  }

  var isValidQuantity = function(value) {
    if(value === '') return false
    return true
  }

  var isValidDescription = function(value) {
    if(value === '') return false
    return true
  }

  var isValidContentFob = function(value) {
    if(value === 0) return false
    return true
  }

  /* Returns false if there is no input found
   */
  function validateNotNull(value){
    if(value === ''){
      return false
    }
  }

  return {

    setPrefixes: function(prefixList){
      self.prefixes = prefixList
      console.log("SETTING UP Prefixes")
    },

    setMeasures: function(measureList){
      self.measures = measureList
      console.log("SETTING UP Measures")
    },

    setCurrencies: function(currencyList){
      self.currencies = currencyList
      console.log("SETTING UP Currencies")
    },

    getFob: function(itemData) {
      if(itemData.currency !== 'USD') {
        var currency = this.getCurrencies().filter(function(a){
          return a.name === itemData.currency
        })
        return (currency[0].value * itemData.fobAmount).toFixed(2)
      } else {
        return itemData.fobAmount
      }
    },

    getCurrencyByName: function(currencyName) {
      return this.getCurrencies().filter(function(a){
        return a.name === currencyName
      })
    },

    getCurrencies: function() {
      return self.currencies
    },

    getPrefixes: function() {
      return self.prefixes
    },

    getMeasures: function() {
      return self.measures
    },

    getValidations: function() {
      return fieldValidations
    },

    /* Purp.: Determines if a shipping is ready to be added
     * Specifically used for new shipping Inputs in the input-form
     *  where
     *        shipping === { shipping }
     */
    isValidInput: function(shipping) {
      return (validateIndex(shipping.data.prefix, shipping.data.id))
    },

    /* Purp.: Evaluates recent changes to determine if a value is valid
     * to be updated for a given property .
     *
     *  where 
     *        value    === { data['somePropertyValue'] -> String || Integer || Double }
     *        property === 'attribute name to update'
     */
    shouldUpdate: function(value, property){
      if(hasChanged(value)) {
        var validationFunction = getFunctionByProperty(property)
        if(validationFunction === null) {
          console.log("Unable to update. Invalid property '" + property + "'.");
          return;
        }
        else {
          return validationFunction()(value);  
        }
      } else {
        FeedbackService.handleFeedback('idle-db')
        return false
      }
    },

    /* Purp.: Evaluates recent content changes to determine if a value is valid
     * to be updated for a given property .
     *
     *  where 
     *        value    === { data['somePropertyValue'] -> String || Integer || Double }
     *        property === 'attribute name to update'
     */
    shouldUpdateContent: function(value, property){
      if(hasChangedContent(value)) {
        var validationFunction = getFunctionByProperty(property)
        if(validationFunction === null) {                                         //should never happen
          console.log("Unable to update. Invalid property '" + property + "'.");
        }
        else {
          return validationFunction();
        }
      } else {
        FeedbackService.handleFeedback('idle-db')
        return false
      }
    }    

  }

})

app.controller('ValidationCtrl', function($scope, ValidationService, InputService, ShippingModel, ContentModel) {

	/* Static cap references
	 */
  const limit = { 
  								weight: 50,
  								fob: 1000
  				  		}

  /* Purp.: Returns a list of prefix name
   * Used in any shipping list to render prefix <option>
   */
  $scope.getPrefixNames = function() {
    return ValidationService.getPrefixes().map(function(a){
      return a.name
    });
  }

  /* Purp.: Returns a list of currency names
   */
  $scope.getCurrencies = function() {
    return ValidationService.getCurrencies().map(function(a){
      return a.name
    })
  }

  $scope.getFob = function(itemData){
    return ValidationService.getFob(itemData)
  }

  $scope.getMeasures = function() {
    return ValidationService.getMeasures()
  }          


  /* Purp.: Determines the current element properties to assign a suitable ng-class
   *  where
   *        index    === element position -> Integer
   *        value    === { item.data }
   *        property === 'attribute name to validate'
   */
	$scope.validateElement = function(index, value, property) {
		return this.isFocused(index) && ValidationService.getValidations()[property](value);
	}

  /* Purp.: Determines if the value given is equal to the current focused id
   * Used to determine if the dom element should trigger a css class
   *  where 
   *        id === 'dom element assigned $index value'
   */
  $scope.isFocused = function(id) {
    return ShippingModel.getDomElementId() === id;
  }  

	

});