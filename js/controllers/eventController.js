
var event = angular.module("EventModule", [])

event.$inject = ['$rootScope', '$scope']

event.$inject = ['AppModel', 'ShippingListModel', 'ShippingModel', 'ContentModel']
event.$inject = ['DBService','InputService', 'ValidationService', 'FeedbackService']

event.factory('EventService', function($rootScope, 
																			 AppModel, ShippingListModel, ShippingModel, ContentModel,
																			 DBService, InputService, ValidationService, FeedbackService){


	function updateContentModel(id, callback) {
		DBService.getContentById({ id:id }, function(content){
/*			var currentAmount = content.reduce(function(a,b){
				return a + parseFloat(b.fobAmount)
			}, 0)
			content['fobAmount'] = ContentModel.getShippingInfo('fob') - currentAmount*/
			$rootScope.$apply(ContentModel.setContent(content))
			callback(content.length)
		})
	}

	return {

		onNewShippingInputView: function(){
			$rootScope.$apply(AppModel.toggleInputForm())
		},

		onShippingView: function(navSetFunction){
			navSetFunction(AppModel.getShippingRows())
			ContentModel.refreshModel()
			AppModel.hideContentInputForm()
			$rootScope.$apply()
		},

		onContentView: function(currentCursor, setNavigationCallback){
			AppModel.setLastBlurredElementId(currentCursor)
			// Keeps a reference to the last focused element and retrieves the current shipping content
			InputService.getShippingContent(ShippingModel.getShippingId(), function(content){
				ContentModel.setContent(content)
				setNavigationCallback(content.length)
			})
			// Provides basic shipping information for the Content Model
	    DBService.getShippingById({ id:ShippingModel.getShippingId() }, function(res){
	      data = res[0].data
	      data['fob'] = ValidationService.getFob(data)
	      data['currencyValue'] = ValidationService.getCurrencyByName(data.currency)[0].value
	      ContentModel.setShippingInfo(data)
	      AppModel.showContentInputForm();
	      $rootScope.$apply()
	    })
		},

		addShipping: function(newShipping){
			InputService.addShipping(newShipping, function(err, shipping){
				if(!err){
					FeedbackService.handleFeedback('shipping-added', shipping.id)
					ShippingListModel.addShipping(shipping)
				}
				else throw err
			})
		},

		removeShipping: function(shippingId){
			DBService.remove(shippingId, function(err,res){
				if(!err){
					FeedbackService.handleFeedback('shipping-deleted', shippingId)
					ShippingListModel.removeShipping(shippingId)
				}
				else {
					FeedbackService.handleFeedback('shipping-does-not-exist')
				}
			})
		},

		addShippingContent: function(updateNavigationCallback){
			if(ContentModel.isVisible()){
				var id    = ContentModel.getId()
				var index = ContentModel.getLength() + 1
				var fob   = ContentModel.getRemainingFob()
				InputService.addShippingContent(id, index, fob, function(){
					updateContentModel(id, function(contentLength){
						updateNavigationCallback(contentLength, contentLength)
					})
				})

			}
		},

		deleteShippingContent: function(updateNavigationCallback){
			if(ContentModel.isVisible()) {
				var id    = ContentModel.getId()
				var index = ContentModel.getIndex()
				InputService.deleteShippingContent(id, index, function(){
					updateContentModel(id, function(contentLength){
						if(index > contentLength){
							index = contentLength
						}
						updateNavigationCallback(contentLength, index)
					})
				})
			}
		}

	}

})

event.$inject = ['EventService']

event.controller('EventCtrl', function($scope, EventService){

	$scope.addShipping = function(){
		EventService.addShipping(this.newShipping)
		this.newShipping = {}
	}

	$scope.removeShipping = function(shippingId){
		EventService.removeShipping(shippingId)
	}


})