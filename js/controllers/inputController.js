
var app = angular.module('InputModule', ['ngRoute', 'ngMaterial', 'ngMessages', 'ValidationModule']);

app.$inject = ['$scope', '$document', 'InputService', 'ValidationService', 'DBService', 'FeedbackService', 
               'AppModel', 'ShippingModel', 'ContentModel', 'NavigationService']

app.factory('InputService', function(AppModel, ShippingModel, ContentModel, ValidationService, DBService, FeedbackService){


  /* Initializes the model used for new shipping inputs
 */
  /*var newShipping = refreshShipping();*/

  /* Purp.: Updates the id reference of the newShipping input model
   */
  function assignIndex(shipping){
    shipping['id'] = shipping.data.prefix + shipping.data.id
  }

  /* Returns an empty shipping ready to edit
   * id_1 -> index
   * id   -> id used for basic querying
   * data -> shipping information
   *
   * Rep. Inv.: id === (data.prefix + data.id)
   */
/*  function refreshShipping() {
    return {
      id: null,
      data: {
        prefix: null,
        id: null,
        packages: null,
        weight: null,
        currency: null,
        fobAmount: null,
        fob: null,
        consignee: null,
        shipper: null
      }
    }
  }*/

  function newContentInput(id, pos, fob) {
    return [{
      id: id,
      pos: pos,
      data: {
        measure: "7",
        quantity: null,
        nadi: null,
        description: null,
        contentFob: fob
      }
    }]
  }


  /* Purp.: Assigns an empty content to the scope variable and updates the current position
   *  where
   *        id === 'shipping id'
   */
  function setNewContent(id){
    console.log("New Content Entry")
    self.maxPos     = 1;
    self.currentPos = 1;
    var content = newContentInput(id,1)
    ContentModel.setContent(content)
  }

  /* Purp.: Assigns a list of existing contents to the scope variable and updates the current position
   *  where
   *        res === [ { shipping content } ]
   */
  function setExistingContent(res){
    console.log("Shipping with content")
    self.maxPos     = res.length
    self.currentPos = 1
    ContentModel.setContent(res)
  }


  return {

    getNewShippingPrefix: function(){
      return newShipping.data.prefix
    },

    getNewShippingId: function(){
      return newShipping.data.id
    },

    /* Purp.: Adds a new shipping
     */
    addShipping: function(newShipping, callback) {
      if(ValidationService.isValidInput(newShipping)) {
        assignIndex(newShipping);
        DBService.add(newShipping, function(err, shipping){
          callback(err,shipping)
        });  
      }
    },

    /* Purp.: Retrieves the content of the current shipping
     *
     */
    getShippingContent: function(id, callback) {
      DBService.getContentById({ id: id }, function(res){
        if(res.length === 0) {
          var content = newContentInput(id,1)
          console.log("new input inserted")
          console.log(content)
          callback(content)
        } else {
          callback(res)
        }
      })
    },

    /* Purp.: Modifies a shipping by index with a new prefix + id 
     *  where 
     *        index    === 'unique document id' (prefix+id)
     *        prefix   === 'prefix'
     *        id       === 'id'
     *        property === 'attribute to update'
     *        model    === { shipping } Used in validations controller callback
     *        callback -> returns to 'onIndexBlur'
     */
    modifyShippingId: function(index, prefix, id, property, model, callback) {
      var shipping       = {};
      var val            = {};
      shipping['id']     = index;
      val['id']          = prefix+id;
      val['data.prefix'] = prefix;
      val['data.id']     = id;

      DBService.modify(shipping, val, property, function(err,res){
        if(err){
          FeedbackService.handleFeedback(err, index);
        } else if(res){
          FeedbackService.handleFeedback(res, index, property, val['data.'+property])
        }
        callback(err,res);
      });
      DBService.updateContentId({ id: index }, { id: val.id })
    },

    /* Purp.: Modifies a shipping by index with a new value on a given property
     *  where 
     *        index    === 'unique document id' (prefix+id)
     *        value    === 'value to update'
     *        property === 'attribute to update'
     *        callback -> returns to 'onBlur'
    */
    modifyShipping: function(index, value, property, callback) {
      var shipping   = {};
      var val        = {};
      var prop       = "data."+property;
      shipping['id'] = index;
      val[prop]      = value;

      DBService.modify(shipping, val, prop, function(err,res){
        if(err) {                                                             //pendiente de handle
          throw err
        } else if(res){
          FeedbackService.handleFeedback(res, index, property, value);
        }
      });
    },

    /* Purp.: Modifies a content with a new value on a given property
     *  where 
     *        value    === 'value to update'
     *        property === 'attribute to update'
     *        callback -> returns to 'onContentBlur'
    */
    modifyShippingContent: function(id, pos, value, property, callback) {
      var shipping    = {};
      var val         = {};
      var prop        = "data."+property;
      shipping['id']  = id;
      shipping['pos'] = pos;
      val[prop]       = value;
      DBService.modifyContent(shipping, val, prop, function(err, res){
        if(err){
          throw err
        }
        else {
          FeedbackService.handleFeedback('content-update', shipping.id, property, val[prop], shipping.pos)
        }
      })
    },

    /* Purp.: Adds an empty content to the database
     *  where
     *        id       === { shippingId }
     *        index    === Integer (current content position)
     *        callback === back to EventModule
     */
    addShippingContent: function(id, index, fob, callback) {
      var newContent = newContentInput(id, index, fob)
      DBService.insertContent(newContent)
      callback()
    }, 

    /* Purp.: Deletes content from the current shipping Id
     *  where
     *        id       === { shippingId }
     *        index    === Integer (current content position)
     *        callback === back to EventModule
     */
    deleteShippingContent: function(id, index, callback) {
      DBService.removeContent(id, index, function(){
        callback()
      })
    }

  }


})

app.$inject = ['ShippingListModel']

app.controller('InputCtrl', function($scope, $document, 
  InputService, ValidationService, DBService, AppModel, NavigationService, ShippingListModel) {

  /* Adds the current shipping, then refreshes the scope */
  $scope.addShipping2 = function() {
    InputService.addShipping()

    ShippingListModel.sortList()
  }

  /* Purp.: Returns the sum of a shippings property
   *  where 
   *        shippings === [ { shipping } ]
   *        property  === 'shipping attribute'
   */
  $scope.getSum = function(shippings, property) {
    return (Math.round(shippings.reduce(function(a,b){
      return a + parseFloat(b.data[property]);
    }, 0) * 1000 ) / 1000).toFixed(2)
  }


});

/*
    $scope.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
    'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
    'WY').split(' ').map(function(state) {
        return {abbrev: state};
      });
*/