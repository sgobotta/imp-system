
var app = angular.module('FeedbackModule', []);

app.factory('FeedbackService', function(){

  const feedback =  {
                        11001: {
                          message1: "Can't apply changes. Shipping with id: ", 
                          message2: " already exists.",
                          kind: "Error.",
                          info: "Duplicated Key.",
                          output: function(id){ console.log(this.message1 + id + this.message2) }
                        },
                        updated: {
                          message1: " updated on ",
                          message2: " with value '",
                          message3: "'.",
                          output: function(id, property, value) { console.log(id + this.message1 + property + this.message2 + value + this.message3) }
                        },
                        deletedContent: {
                          message1: "Content for id '", 
                          message2: "' on pos '",
                          message3: "' was deleted.",
                          output: function(id, pos) { console.log(this.message1 + id + this.message2 + pos + this.message3) }
                        },
                        updatedContent: {
                          message1: "Content of id '",
                          message2: "' at pos '",
                          message3: "' was updated on '",
                          message4: "' with value '",
                          message5: "'.",
                          output: function(id, pos, property, value) { console.log(this.message1 + id + this.message2 + pos + this.message3 + property + this.message4 + value + this.message5) }
                        },
                        addedShipping : {
                          message1: "Shipping inserted with id '",
                          message2: "'.",
                          output: function(id) { console.log(this.message1 + id + this.message2) }
                        },
                        deletedShipping: {
                          message1: "Shipping with id '",
                          message2: "' has been removed.",
                          output: function(id) { console.log(this.message1 + id + this.message2) }
                        },
                        inexistentShipping: {
                          message1: "Shipping with id '",
                          message2: "' does not exist.",
                          output: function(id) { console.log(this.message1 + id + this.message2) }
                        }

                      }

  return {

    /* Receives an object to log relevant information
     *  where obj === Error || Result
     */
                                                                            // Todavía le falta pulir polimorfismo
    handleFeedback: function(obj, shippingId, property, value, extra){
      // Receives a MongoError
      if(obj['name'] === 'MongoError'){
        var feed = feedback[obj.code] 
        feed.output(shippingId);
      }
      // Receives a CommandResult from Mongo
      if(obj.modifiedCount === 1) {
        var feed = feedback['updated']
        feed.output(shippingId, property, value)
      }
      if(obj === 'idle-db'){
        var feed = feedback['obj']
        /*console.log("no changes")*/
      }
      if(obj === 'content-update'){
        var feed = feedback['updatedContent']
        feed.output(shippingId, extra, property, value)
      }
      if(obj === 'content-deleted'){
        var feed = feedback['deletedContent']
        feed.output(shippingId, extra)
      }
      if(obj === 'shipping-added'){
        var feed = feedback['addedShipping']
        feed.output(shippingId)
      }
      if(obj === 'shipping-deleted'){
        var feed = feedback['deletedShipping']
        feed.output(shippingId)
      }
      if(obj === 'shipping-does-not-exist'){
        var feed = feedback['inexistentShipping']
        feed.output(shippingId)
      }
    }

  }

})

/* Controller mainly used to display information to the user, such as
 * errors and positive input results.
 */
app.controller('FeedbackCtrl', function($scope) {

	const feedback =  {
												11001: {
													message1: "Can't apply changes. Shipping with id: ", 
													message2: " already exists.",
													kind: "Error.",
													info: "Duplicated Key.",
													output: function(id){ console.log(this.message1 + id + this.message2) }
												},
												updated: {
													message1: " updated on ",
													message2: " with value '",
													message3: "'.",
													output: function(id, property, value) { console.log(id + this.message1 + property + this.message2 + value + this.message3) }
												},
                        deletedContent: {
                          message1: "Content for id '", 
                          message2: "' on pos '",
                          message3: "' was deleted.",
                          output: function(id, pos) { console.log(this.message1 + id + this.message2 + pos + this.message3) }
                        }
											}


  /* Receives an object to log relevant information
   * 	where obj === Error || Result
   */
                                                                          // Todavía le falta pulir polimorfismo
  $scope.handleFeedback = function(obj, shippingId, property, value){
  	// Receives a MongoError
  	if(obj['name'] === 'MongoError'){
  		var feed = feedback[obj.code]	
  		feed.output(shippingId);
  	}
  	// Receives a CommandResult from Mongo
  	if(obj.modifiedCount === 1) {
  		var feed = feedback['updated']
  		feed.output(shippingId, property, value)
  	}
  	if(obj === 'idle-db'){
  		var feed = feedback['obj']
  		console.log("no changes")
  	}
  }

  $scope.feed = function(type, id, pos) {
    var feed = feedback[type]
    feed.output(id, pos);
  }

});


/*MongoError {name: "MongoError", message: "E11000 duplicate key error index: ShippingDB.shippingDocuments.$id_1  dup key: { : "US00005463" }", driver: true, ok: 1, n: 0…}
code:11001
driver:true
errmsg:"E11000 duplicate key error index: ShippingDB.shippingDocuments.$id_1  dup key: { : "US00005463" }"
message:"E11000 duplicate key error index: ShippingDB.shippingDocuments.$id_1  dup key: { : "US00005463" }"
n:0
nModified:0
name:"MongoError"
ok:1*/