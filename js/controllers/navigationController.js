

var nav = angular.module('NavigationModule', ['electangular']);


nav.$inject = ['$rootScope', '$scope', '$document', '$timeout', 'electron', 'AppModel', 'ShippingModel', 'ContentModel', 'InputService', 'ValidationService', 'EventService']

nav.factory('NavigationService', function($rootScope, $document, electron, EventService, AppModel){

  const remote         = require('electron').remote
  const GlobalShortcut = electron.globalShortcut
  var win              = remote.getCurrentWindow()

  /* Navigation context */
  var self = this;

  /* Shipping navigation constant data */
  const shippingColumns = 10
  const shippingPrefix  = 's'

  /* Content navigation constant data */
  const contentColumns  = 6
  const contentPrefix   = 'c'

  /* Navigation coordenates */
  var columns = null
  var rows    = null

  /* Navigation variable data */
  self.navPrefix             = shippingPrefix
  self.shippingRows          = null
  self.contentRows           = null
  self.cursor                = 1
  self.currentContext        = 'shipping'


  /* Purp.: Initializes navigation data to handle a shipping list context
   *  where
   *        numberOfRows === quantity of current shippings
   */
  var setShippingStage = function(numberOfRows) {
    registerShippingKeys()
    self.cursor = AppModel.getLastBlurredElementId()
    self.navPrefix      = shippingPrefix
    self.shippingRows   = numberOfRows
    rows    = self.shippingRows
    columns = shippingColumns
    moveToCurrent()
  }

  /* Purp.: Modifies navigation data to handle a content context
   *  where
   *        shippingId   === shipping unique id
   *        numberOfRows === quantity of current content items
   *        element      === DOM element id
   */
  var setContentStage = function(numberOfRows) {
    registerContentKeys()
    self.contentRows = numberOfRows

    self.cursor  = 2
    self.navPrefix  = contentPrefix
    rows            = self.contentRows
    columns         = contentColumns
  }

  /* Purp.: 
   * Prec.: The current state must be a Content context
   */
  var updateContentStage = function(numberOfRows, cursor) {
    self.contentRows = numberOfRows
    rows = self.contentRows
    self.cursor = (cursor*contentColumns) - (contentColumns-2)
    moveToCurrent()
  }

  /* Purp.: Assigns a reference to the next column
   */
  function nextCol(){
    self.cursor = self.cursor + 1
  }

  /* Purp.: Assigns a reference to the previous column
   */
  function prevCol(){
    self.cursor = self.cursor - 1
  }

  /* Purp.: Assigns a reference to the next row
   */
  function nextRow(){
    self.cursor = self.cursor + columns
  }

  /* Purp.: Assigns a reference to the previous row
   */
  function prevRow(){
    self.cursor = self.cursor - columns
  }

  /* Purp.: Determines if the current id is at the first column
   */
  function atFirstCol(){
    return (self.cursor-1) % columns === 0
  }

  /* Purp.: Determines if the current id is at the last column
   */
  function atLastCol(){
    return self.cursor % columns === 0
  }

  /* Purp.: Determines if the current id is at the first row
   */
  function atFirstRow(){
    return (self.cursor - columns) <= 0
  }

  /* Purp.: Determines if the current id is at the last row
   */  
  function atLastRow() {
    return (self.cursor + columns) > (rows*columns)
  }

  /* Purp.: Moves to the current element
   */
  function moveToCurrent() {
    /*console.log("cursor: " + self.cursor+self.navPrefix)*/
    var element = $document[0].getElementById(self.cursor+self.navPrefix)
    if(element.tagName == "INPUT"){
      element.select()
    } else if(element.tagName == "SELECT" || element.tagName == "BUTTON"){
      element.focus()
    }
    $rootScope.$apply()
  }

  function moveToFirst() {
    $document[0].getElementById(1+self.navPrefix).focus()
  }

  /* Purp.: Moves to the previous column
   */
  function MoveLeft() {
    if(!atFirstCol()) {
      prevCol()
      moveToCurrent();
    }   
  }

  /* Purp.: Moves to the next column
   */
  function MoveRight() {
    if(!atLastCol()) {
      nextCol()
      moveToCurrent();
    } 
  }

  /* Purp.: Moves to the previous row
   */
  function MoveUp() {
    if(!atFirstRow()){
      prevRow()
      moveToCurrent();
    }
  }

  /* Purp.: Moves to the next row
   */
  function MoveDown() {
    if(!atLastRow()) {
      nextRow()
      moveToCurrent();
    }
  }

  /* Binding configurations
   */
  const registerBinding = {
    content: function() {
      registerContentKeys()
    },
    shipping: function() {
      registerShippingKeys()
    }
  }   

  /* Purp.: Registers basic movement across the grill
   */
  function registerMovementKeys() {
    GlobalShortcut.register('Left',  () => { MoveLeft()  });
    GlobalShortcut.register('Right', () => { MoveRight() });
    GlobalShortcut.register('Up',    () => { MoveUp()    });
    GlobalShortcut.register('Down',  () => { MoveDown()  });    
  }

  /* Registers specific key bindings for a shipping content input context
   */
  function registerContentKeys() {
    self.currentContext = 'content'
    GlobalShortcut.unregisterAll();
    registerMovementKeys()
    GlobalShortcut.register('Esc',    () => { EventService.onShippingView(setShippingStage) })
    GlobalShortcut.register('Delete', () => { EventService.deleteShippingContent(updateContentStage) })
    GlobalShortcut.register('Insert', () => { EventService.addShippingContent(updateContentStage) })
  }

  /* Registers specific key bindings for a shipping input context
   */
  function registerShippingKeys() {
    self.currentContext = 'shipping'
    GlobalShortcut.unregisterAll();
    registerMovementKeys()
    GlobalShortcut.register('F6',         () => { EventService.onNewShippingInputView() })
    GlobalShortcut.register('Control+C',  () => { EventService.onContentView(self.cursor, setContentStage) })
  }
 

  /* Determines blur behaviour for the current browser window
   */
  win.on('blur', () => {
    GlobalShortcut.unregisterAll();
  })

  /* Determines focus behavious for the current browser window
   */
  win.on('focus', () => {
    registerBinding[self.currentContext]()
  })

  return {

    initialize: function(numberOfRows){
      registerShippingKeys()
      self.cursor = AppModel.getLastBlurredElementId()
      self.navPrefix      = shippingPrefix
      self.shippingRows   = numberOfRows
      rows    = self.shippingRows
      columns = shippingColumns
    },

    setCursor: function(cursor){
      self.cursor = cursor
    },

    getSetShippingStateFunction: function(){
      return setShippingState
    },

    getSetContentStateFunction: function(){
      return setContentState
    }


  }


})

nav.controller('NavigationCtrl', function($scope, $document, $timeout, electron, 
                              AppModel, ShippingModel, ContentModel, InputService, ValidationService, EventService) {

  /* Purp.: Updates the Shipping Model
   *
   *  where 
   *        id        === { 'id' }
   *        value     === { 'data.prefix' || 'data.id' -> String/Integer }
   *        data      === { shipping.data }
   *        elementId === 'dom element assigned $index value'
   *
   */
  $scope.onIdFocus = function(id, value, data, elementId) {
    ShippingModel.setIdModel(id, value, data, elementId)
  }

  /* Purp.: Updates if necesary any relevant index input
   *
   *  where 
   *        index    === { 'id' -> String } *Document Index* (prefix+id)
   *        prefix   === { 'data.prefix' -> String }
   *        id       === { 'data.id'     -> Integer}
   *        property === 'attribute name to update'
   *        model    === { shipping.data }
   */
  $scope.onIdBlur = function(index, prefix, id, property, model) {
    var value = { prefix: function(){return prefix}, id: function(){return id} }

    if(ValidationService.shouldUpdate(value[property](), property)) {
      var currentFocus = ShippingModel.getDataId()
      InputService.modifyShippingId(index, prefix, id, property, currentFocus, function(err,res) {
        if(err){
          model['data'][property] = currentFocus[property]
        } else if(res){
          model['id'] = prefix+id;    
        }
      });
    } else {
      /*model['data'][property] = $scope.currentFocus['currentId'][property]; */
      //nunca sucede para que siempre se actualice id
    }
  }

  /* Purp.: Assigns current shipping data values temporarily
   *
   *  where 
   *        id        === { 'id' }
   *        value     === { data['somePropertyValue'] !== ('data.prefix' || 'data.id') }
   *        elementId === 'dom element assigned $index value'
   *
   */
  $scope.onFocus = function(id, value, elementId) {
    ShippingModel.setModel(id, value, elementId)
  }

  /* Purp.: Updates if necesary any relevant data input
   *
   *  where 
   *        index    === { 'id' -> String } *Document Index* (prefix+id)
   *        value    === { data['somePropertyValue'] !== ('data.prefix' || 'data.id') -> String || Integer || Double }
   *        property === 'attribute name to update'
   *        model    === { shipping.data }
   */
  $scope.onBlur = function(id, value, property, model) {
    if(ValidationService.shouldUpdate(value, property)) {

      //formatear value dependiendo la property: EJ: si es un int, parsearlo a Int, porque no queremos guardar precios en string
      InputService.modifyShipping(id, value, property, function(err,res){
        console.log("pendiente de callback")
      });
    }
    else {
      model[property] = ShippingModel.getValue()
    }
  }

  /* Purp.: Assigns current shipping content data values temporarily
   * 
   *  where
   *        id        === 'shippingId'
   *        index     === 'content position'
   *        value     === current data[property] value
   *        elementId === DOM element id
   *
   */
  $scope.onContentFocus = function(id, index, value, elementId) {
    ContentModel.setModel(id, index, value, elementId)
  }

  /* Purp.: Updates if necesary any relevant content input and refreshes scope variables
   *
   *  where
   *        value    === { data['somePropertyValue'] }
   *        property === 'attribute name to update'
   *        model    === { shippingContent.data }
   *
   */
  $scope.onContentBlur = function(value, property, model) {
    if(ValidationService.shouldUpdateContent(value, property)) {
      InputService.modifyShippingContent(ContentModel.getId(), ContentModel.getIndex(), value, property, function(err,res){
        console.log("pendiente de callback")
      });
    }
    else {
      model[property] = ContentModel.getValue()
    }
  }

});

/* Purp.: Determines the element to be focused
 */
/*app.directive('showFocus', function($timeout) {
  return function(scope, element, attrs) {
    scope.$watch(attrs.showFocus, function(newValue) {     	
      $timeout(function() {
        newValue && element.focus()
        
  		});
    },true);
  };    
});
*/
