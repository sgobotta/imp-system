
var database = angular.module('DBModule', ['SortingControllers', 'MongoDAO']);

database.$inject = ['$scope', 'DAOService', 'DBService', 'FeedbackService'];

database.factory('DBService', function(DAOService, FeedbackService){

	const MongoClient = require('mongodb').MongoClient
		// Connection URL
	const shippingsDB = 'mongodb://127.0.0.1:27017/ShippingDB'

	const shippingDocuments = "shippingDocuments"
	const shippingContent   = "shippingContent"
	const currencyDocument  = "currency"
	const shippingPrefixes  = "shippingPrefixes"
	const measureDocument   = "contentMeasures"

	/* Purp.: Creates a new collection with a unique index by id
	 * 	where
	 *				name === 'databaseName'
	 */
	function createCollection(name){
		MongoClient.connect(shippingsDB, function(err, db) {
			db.createCollection(name, function(err, collection){
				if(!err) {
					collection.ensureIndex( { 'id':1 },
																	{ unique:true },
																	{ background:true },
																	{ dropDups:true } );
				} else throw err;
			});
		})
	}

	/* Purp.: Creates a new collection
	 * 	where
	 *				name === 'databaseName'
	 */
	function createNonIndexedCollection(name) {
		MongoClient.connect(shippingsDB, function(err, db) {
			db.createCollection(name, function(err, collection){
			});
		})		
	}

	// Does nothing if the collection already exists
	createNonIndexedCollection("shippingContent");
	createNonIndexedCollection("currency")
	createNonIndexedCollection("shippingPrefixes")
	createNonIndexedCollection("contentMeasures")

	createCollection("shippingDocuments")


	/* Purp.: Recives a path to connect to a database
	 * 	where dbPath   === 'url'
	    			callback === returns results to 'modify'
	 */
	function getCollection(collectionName, callback) {
		MongoClient.connect(shippingsDB, function(err, db) {
			if(!err){
				callback(db.collection(collectionName), function(updateErr, updateRes){})
				db.close()
			}
			else {
				console.log(err);
			}
		})
	}

	/* Purp.: Updates every content 'pos' property
	 *	where
	 *				contentId === 'shipping Id'
	 *				pos       === 'shipping position'
	 */
	function decrementContentPos(contentId, pos, callback) {
		getCollection(shippingContent, function(collection){
			DAOService.updateManyWith(collection, contentId, pos, { pos: -1 }, function(err,res){
				if(!err){
					console.log("Content positions were updated")
					callback()
				} else {
					console.log("Deleted content with id: " + contentId + " and pos: " + pos + ", but could not update positions")
					throw err
				}
			})
		})
	}			


	return {

		/* Purp.: Inserts all elements of a given object list
		 * 	where
		 *				data === [ { shipping } ]
		 */
		importList: function(data, collectionName) {
			MongoClient.connect(shippingsDB, function(err, db) {
				if(!err) {
					var col = db.collection(collectionName);
					col.insertMany(data, function(err, res){
						if(!err){
							console.log(res)
						} else throw err
					})
				} else throw err
			})
		},

		/* Purp.: Adds an element to a collection
		 * 	where 
		 *				shipping === { shipping }
		 */
		add: function(shipping, callback) {
			getCollection(shippingDocuments, function(collection) {
				DAOService.insert(collection, shipping, function(err,res){
					callback(err,shipping)
				})
			})
		},

	  /* Purp.: Gets all shippings from database
	   */
		getAllShippings: function(callback) {
			getCollection(shippingDocuments, function(collection) {
				DAOService.findAll(collection, function(err,res){
					callback(res)
				})
			})
		},

		/* Purp.: Updates a shipping with a value
		 * 	where 
		 *				shippingId === { id }
		 *			  value 		 === { attributes and values }
		 *			  property   === 'attribute to update'
		 *        callback   === returns values to *modifyShipping*, *modifyShippingId* in 'inputController'
		 */
		modify: function(shippingId, value, property, callback) {
			getCollection(shippingDocuments, function(collection){
				DAOService.update(collection, shippingId, value, property, function(err,res){
					callback(err,res)
				})
			})
		},

		/* Purp.: Removes a shipping from a collection
		 * 	where 
		 *				shippingId === { id }
		 */
		remove: function(shippingId, callback) {
			getCollection(shippingDocuments, function(collection) {
				DAOService.delete(collection, shippingId, function(err,res){
					callback(err,res)
				})
			})
		},

		/* Purp.: Inserts a new content
	   *	where
	   *				content === { content }
		 */
		 insertContent: function(content) {
		 	getCollection(shippingContent, function(collection){
		 		DAOService.insert(collection, content, function(err,res){	
		 			if(!err){
		 																		// FEEDBACK CTRL PLS
		 			} else throw err
		 		})
		 	})
		 },

		/* Purp.: Gets all documents of a given id
		 * 	where
		 *				id === { id }
		 *        callback -> returns values to 'inputController'
		 */
		getContentById: function(id, callback) {
			getCollection(shippingContent, function(collection){
				DAOService.findById(collection, id, function(err,res){
					if(!err) {
						callback(res)
					} else throw err
				})
			})
		},

		/* Purp.: Updates all contents id of id 'index' with a given 'value'
		 *	where
		 *				index === { id }
		 *				value === { newId }
		 */
		updateContentId: function(index, value) {
			getCollection(shippingContent, function(collection){
				DAOService.updateMany(collection, index, value, function(err,res){
					if(!err) {
					} else throw err
				})
			})
		},

		/* Purp.: Updates a shipping content with a value on a property
		 *	where
		 *				shippingId === { contentId }
		 *				value      === { object to update }
		 *			  property   === 'attribute to update'
		 *				callback -> returns to 'inputController'
		 */
		modifyContent: function(shippingId, value, property, callback) {
			getCollection(shippingContent, function(col){
				DAOService.update(col, shippingId, value, property, function(err,res){
					callback(err,res)
				})
			})
		},

		/* Purp.: Removes a shipping content from a collection
		 * 	where 
		 *				contentId 		 === 'shipping Id'
		 *				pos       		 === 'shipping position'
		 */
		removeContent: function(contentId, pos, callback) {
			getCollection(shippingContent, function(collection) {
				DAOService.delete(collection, {id: contentId, pos: pos}, function(err,res){
					if(!err){
						FeedbackService.handleFeedback("content-deleted", contentId, null, null, pos)
						decrementContentPos(contentId, pos, callback)
					} else throw err
				})
			})
		},

		/* Purp.: Given a shipping id returns the name of it's consignee
		 * 	where
		 *				id === { id }
		 */
		getShippingById: function(id, callback) {
			getCollection(shippingDocuments, function(collection){
				DAOService.findById(collection, id, function(err,res){
					if(!err) {
						callback(res)
					}
					else throw err
				})
			})
		},

		/* Purp.: Gets all the prefixes
		 */
		getAllPrefixes: function(callback) {
			getCollection(shippingPrefixes, function(collection){
				DAOService.findAll(collection, function(err,res){
					if(!err){
						callback(res)
					}
					else throw err
				})
			})
		},

		/* Purp.: Gets all the currencies from database
		 */
		getAllCurrencies: function(callback) {
			getCollection(currencyDocument, function(collection){
				DAOService.findAll(collection, function(err,res){
					if(!err){
						callback(res)
					}
					else throw err
				})
			})
		},

		/* Purp.: Gets the content measures
		 */
		getContentMeasures: function(callback) {
		 	getCollection(measureDocument, function(collection){
		 		DAOService.findAll(collection, function(err,res){
		 			if(!err){
		 				callback(res)
		 			}
		 			else throw err
		 		})
		 	})
		}

	}


})



	// Shippings Import from JSON

// Usar este a pesar del error de cargar 1 solo item

	
	
/*	fs.readFile('resources/shipping.json', 'utf8', function(err, data){
		content = JSON.parse(data);
		console.log(content['information'].length)
		importList(content['information']);
	});*/
