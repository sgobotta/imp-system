
var parser = angular.module('TextParserModule', [])



parser.factory('TextParserService', function(){

	const fs = require('fs');

	const transf_structure = JSON.parse(fs.readFileSync('resources/ncm/transf_structure.json', 'utf8')).structure
	var transf_sections = Object.keys(transf_structure)

	var notas = {}
	const nota1 = JSON.parse(fs.readFileSync('resources/ncm/notas/nota1.json', 'utf8')).LicenciaNA

	const notasPath = 'resources/ncm/notas/'
	const notasName = ['nota1', 'nota2', 'nota3', 'nota4', 'nota5', 'nota6',
										 'nota7', 'nota10', 'nota12', 'nota14', 'notaAutomotriz']
	const json_extension = '.json'
	const notasPathList  = [	
														notasPath+notasName[0]+json_extension,
														notasPath+notasName[1]+json_extension,
														notasPath+notasName[2]+json_extension,
														notasPath+notasName[3]+json_extension,
														notasPath+notasName[4]+json_extension,
														notasPath+notasName[5]+json_extension,
														notasPath+notasName[6]+json_extension,
														notasPath+notasName[7]+json_extension,
														notasPath+notasName[8]+json_extension,
														notasPath+notasName[9]+json_extension,
														notasPath+notasName[10]+json_extension
												]



function createNotesObject() {
	for(nota in notasName){
		notas[notasName[nota]] = JSON.parse(fs.readFileSync(notasPathList[nota])).structure
	}	
}

createNotesObject()


	/*console.log(notas)*/



/*	var NMCinput = fs.createReadStream('resources/ncm/ncm.txt', 'utf8')

	const rl = require('readline').createInterface({
		input: NMCinput,
		terminal: false
	})*/


	/* Purp.: Calls lineToArray on every line of the text to store the returned content in a list
	 * 				On each call stores an array to nadiArrayList
	 */
	var count = 0
	var nadiJSONList  = []

	// file output path
	const filePath = 'resources/ncm/bbdd/ncm'
	// current chapter
	var chap        = '01'

	var tempChap = ''

/*	rl.on('line', function(line){
		var nadiArray = lineToArray(line)
		var c = nadiArray[0] + nadiArray[1]

		if(c > chap){
			tempChap = c
			rl.pause()
		}
		console.log("pushing to chap '" + chap + "'.")
		nadiJSONList.push(parseNadi(count, nadiArray))
		count++
	})*/

/*	rl.on('pause', function(){
		writeJson(filePath + chap + json_extension)
		chap = tempChap
		rl.resume()
	})*/

	/* Purp.: When the Read Stream finishes shows the results and starts the final parsing
	 */
/*	rl.on('close', function(){
		console.log("Parse finished")
		console.log("wrote " + count + " nadis to " + filePath)
	})*/

	function writeJson(rutaDeArchivo){
		var wstream = fs.createWriteStream(rutaDeArchivo, {flags: 'w'})
		fs.appendFileSync(rutaDeArchivo,"{ ", {encoding:'utf8'})
		for(i=0;i<nadiJSONList.length-1;i++){
			fs.appendFileSync(rutaDeArchivo,'"' + i.toString() + '":', {encoding:'utf8'})
			fs.appendFileSync(rutaDeArchivo, JSON.stringify(nadiJSONList[i]), {encoding: 'utf8'})
			fs.appendFileSync(rutaDeArchivo, ",", {encoding: 'utf8'})
		}
		fs.appendFileSync(rutaDeArchivo,'"' + i.toString() + '":', {encoding:'utf8'})
		fs.appendFileSync(rutaDeArchivo, JSON.stringify(nadiJSONList[i]), {encoding: 'utf8'})
		fs.appendFileSync(rutaDeArchivo, "}", {encoding: 'utf8'})
		console.log("Output: " + filePath + chap + '.json')
		nadiJSONList = []
		wstream.close()
	}


	/* Purp.: Returns an array where the elements are each character of the given text line
	 */
	function lineToArray(line){
		console.log("reading nadi #" + count + "...")
		var res = []
		for(i = 0; i < line.length; i++) {
			res.push(line[i])
		}
		return res
	}

	/* Returns a nadi json object
	 * nadiArray === [character]
	 */
	function parseNadi(nadiArrayIndex, nadi){
		var nadiObj = {}
		for(section in transf_sections){
			var current_section = transf_sections[section]
			for(field in transf_structure[current_section]){
				var current_field = transf_structure[current_section][field]
				nadiObj[field] = parseFunction[parseInt(field)](current_field, nadi)
			}
		}
		return nadiObj
	}	

	function getValue(field, nadiArrayIndex){
		var from = field.from
		var to   = field.to
		var name = field.name
		var obj = {}
		var res = ""
		for(i=from-1; i<to; i++){
			res+=nadiArrayIndex[i]
		}
		obj['name']  = name
		obj['value'] = res
		return obj
	}

	function getValueWithExtraProperty(field, nadiArrayIndex, property){
		var obj = getValue(field, nadiArrayIndex)
		obj[property] = extractObjectPropertyData[property](obj, field[property])
		return obj
	}

	
	const extractObjectPropertyData = {
		obs: function(obj, objectProperty){ return objectProperty },
		ref: function(obj, objectProperty){ 
			/*console.log((notas[objectProperty])[obj.value]);*/
			return (notas[objectProperty])[obj.value]
		}
	}

	const parseFunction = {
		01: function(field,index) { return getValue(field, index) },
		02: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		03: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		04: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		05: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		06: function(field,index) { return getValue(field, index) },
		07: function(field,index) { return getValue(field, index) },
		08: function(field,index) { return getValue(field, index) },
		09: function(field,index) { return getValue(field, index) },
		10: function(field,index) { return getValue(field, index) },
		11: function(field,index) { return getValue(field, index) },
		12: function(field,index) { return getValue(field, index) },
		13: function(field,index) { return getValue(field, index) },
		14: function(field,index) { return getValue(field, index) },
		15: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		16: function(field,index) { return getValue(field, index) },
		17: function(field,index) { return getValue(field, index) },
		18: function(field,index) { return getValue(field, index) },
		19: function(field,index) { return getValue(field, index) },
		20: function(field,index) { return getValue(field, index) },
		21: function(field,index) { return getValue(field, index) },
		22: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		23: function(field,index) { return getValue(field, index) },
		24: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		25: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		26: function(field,index) { return getValue(field, index) },
		27: function(field,index) { return getValue(field, index) },
		28: function(field,index) { return getValue(field, index) },
		29: function(field,index) { return getValue(field, index) },
		30: function(field,index) { return getValue(field, index) },
		31: function(field,index) { return getValue(field, index) },
		32: function(field,index) { return getValue(field, index) },
		33: function(field,index) { return getValue(field, index) },
		34: function(field,index) { return getValue(field, index) },
		35: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		36: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		37: function(field,index) { return getValue(field, index) },
		38: function(field,index) { return getValue(field, index) },
		39: function(field,index) { return getValue(field, index) },
		40: function(field,index) { return getValue(field, index) },
		41: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		42: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		43: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		44: function(field,index) { return getValue(field, index) },
		45: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		46: function(field,index) { return getValue(field, index) },
		47: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		48: function(field,index) { return getValue(field, index) },
		49: function(field,index) { return getValue(field, index) },
		50: function(field,index) { return getValue(field, index) },
		51: function(field,index) { return getValue(field, index) },
		52: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		53: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		54: function(field,index) { return getValue(field, index) },
		55: function(field,index) { return getValue(field, index) },
		56: function(field,index) { return getValue(field, index) },
		57: function(field,index) { return getValue(field, index) },
		58: function(field,index) { return getValueWithExtraProperty(field, index, 'obs') },
		59: function(field,index) { return getValue(field, index) },
		60: function(field,index) { return getValue(field, index) },
		61: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		62: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		63: function(field,index) { return getValue(field, index) },
		64: function(field,index) { return getValue(field, index) },
		65: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		66: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		67: function(field,index) { return getValueWithExtraProperty(field, index, 'ref') },
		68: function(field,index) { return getValue(field, index) },
		69: function(field,index) { return getValue(field, index) },
		70: function(field,index) { return getValue(field, index) },
		71: function(field,index) { return getValue(field, index) },
		72: function(field,index) { return getValue(field, index) },
		73: function(field,index) { return getValue(field, index) }
	}

	return {
		logFile: function(){
			console.log("log")
		},

		getChapter: function(chapter){
			var obj = JSON.parse(fs.readFileSync(filePath+chapter+json_extension, 'utf8'))
			var arr = Object.keys(obj).map(function(k){ return obj[k] })
			console.log(arr)
			return arr
		}
	}

})

parser.$inject = ['$scope', 'TextParserService']

parser.controller('TextParserCtrl', function($scope, TextParserService){

	$scope.getChapter02 = function(){
		return TextParserService.getChapter('02')
	}

})
















	// Trim a la descripción
/*	function getTrimmedDescription(field){

		var obj = getValue(field, index)
		var count = 0
		var delimiter = "-"
		var descr = obj.value
		var newDescr = ""
		var newObj = {}
		for(c in descr){
			if(descr[c] !== delimiter){
				newDescr = newDescr + descr[c]
			} else if(descr[c] === delimiter) {

				var property = "descr"+count
				newObj[property] = newDescr
				newDescr = ""
				delimiter = "- -"
				count++
				console.log(newObj[property])
			}
			if(count === 0){
				obj.value = newDescr
			}
			else {
				obj.value = newObj
			}
		}

			console.log(descr)
			console.log("COUNT: " + count)
			console.log(descr.descr0)

		return obj
	}*/