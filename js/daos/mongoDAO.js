var dao = angular.module('MongoDAO', []);

dao.factory("DAOService", function(){

	return {

		/* Purp.: Inserts many documents in a collection 
		 * 	where 
		 *        shippingList === [ { shipping } ]
		 *        callback -> returns values to 'databaseController'
		 */
		insertMany: function(collection, shippingList, callback) {
			collection.insertMany(shippingList, function(err, res){
				console.log(shippingList)
				callback(err, res)
			})
		},

		/* Purp.: Finds all documents of a given collection
		 * 	where
		 *        callback -> returns values to 'databaseController'
		 */
		findAll: function(collection, callback) {
			collection.find({}).toArray(function(err, elems) {
				callback(err, elems)
			})
		},

		/* Purp.: Finds the first document of a given id on a given collection
		 * 	where
		 * 				id === { shipping.id }	 					
		 *				callback -> returns values to 'databaseController'
		 */
		 findById: function(collection, id, callback) {
		 	collection.find(id).toArray(function(err, res) {
		 		callback(err, res)
		 	})
		 },

	  /* Purp.: Inserts a document in a given collection
	   * 	where 
	   *        shipping === { shipping }
	   *        callback -> returns values to 'databaseController'   
	   */
		insert: function(collection, shipping, callback) {
			collection.insert(shipping, function(err,res){
				callback(err,res)
			})
		},

		/* Purp.: Deletes a document from a given collection
		 * 	where 
		 *        shippingId === { shipping.id }
		 *        callback -> returns values to 'databaseController'
		 */
		delete: function(collection, shippingId, callback) {
			collection.remove(shippingId, function(err, res) {
				callback(err, res)
			})
		},

		/* Purp.: Updates a document value of a given id
		 * 	where 
		 *        shippingId === { id }
		 *				value 		 === { attributes and values }
		 *    	  property   === 'attribute to update'
		 *        callback -> returns values to 'databaseController'
		 */
		update: function(collection, shippingId, value, property, callback) {
			collection.updateOne(shippingId, { $set: value }, { upsert: true }, function(err, res){
				callback(err, res)
			})
		},

		/* Purp.: Updates every document id matching the object condition 
		 *	where 
		 *				shippingId === 'shipping Id'
		 *				value      === 'new Id'
		 *				callback -> returns to updateContentId
		 */
		updateMany: function(collection, shippingId, value, callback) {
			collection.updateMany(shippingId, { $set: value }, function(err,res){
				callback(err,res)
			})
		},

		/* Purp.: Updates every document id matching the object condition with an incremented pos
		 * 	where
		 *				shippingId === 'shipping Id'
		 *				pos        === 'content position'
		 *				value 		 === Integer
		 *				callback -> returns to decrementContentPos
		 */
		updateManyWith: function(collection, shippingId, pos, value, callback) {
			collection.updateMany({id: shippingId, pos: { $gt: pos} }, { $inc: value }, function(err,res){
				callback(err,res)
			})
		}		


	}

})

dao.$inject = ['$scope', 'DAOService'];


// DAOController is a script that includes simple CRUD functions for a database controller
dao.controller('DAOCtrl', function($scope, DAOService) {

	/* Purp.: Inserts many documents in a collection 
	 * 	where 
	 *        shippingList === [ { shipping } ]
	 *        callback -> returns values to 'databaseController'
	 */
	$scope.insertMany = function(collection, shippingList, callback) {
		DAOService.insertMany(collection, shippingList, callback)
	}

	/* Purp.: Finds all documents of a given collection
	 * 	where
	 *        callback -> returns values to 'databaseController'
	 */
	$scope.findAll = function(collection, callback) {
		DAOService.findAll(collection,callback)
	}

	/* Purp.: Finds the first document of a given id on a given collection
	 * 	where
	 * 				id === { shipping.id }	 					
	 *				callback -> returns values to 'databaseController'
	 */
	 $scope.findById = function(collection, id, callback) {
	 	DAOService.findById(collection, id, callback)
	 }

  /* Purp.: Inserts a document in a given collection
   * 	where 
   *        shipping === { shipping }
   *        callback -> returns values to 'databaseController'   
   */
	$scope.insert = function(collection, shipping, callback) {
		DAOService.insert(collection, shipping, callback)
	}

	/* Purp.: Deletes a document from a given collection
	 * 	where 
	 *        shippingId === { shipping.id }
	 *        callback -> returns values to 'databaseController'
	 */
	$scope.delete = function(collection, shippingId, callback) {
		DAOService.delete(collection, shippingId, callback)
	}

	/* Purp.: Updates a document value of a given id
	 * 	where 
	 *        shippingId === { id }
	 *				value 		 === { attributes and values }
	 *    	  property   === 'attribute to update'
	 *        callback -> returns values to 'databaseController'
	 */
	$scope.update = function(collection, shippingId, value, property, callback) {
		DAOService.update(collection, shippingId, value, property, callback)
	}

	/* Purp.: Updates every document id matching the object condition 
	 *	where 
	 *				shippingId === 'shipping Id'
	 *				value      === 'new Id'
	 *				callback -> returns to updateContentId
	 */
	$scope.updateMany = function(collection, shippingId, value, callback) {
		DAOService.updateMany(collection, shippingId, value, callback)
	}

	/* Purp.: Updates every document id matching the object condition with an incremented pos
	 * 	where
	 *				shippingId === 'shipping Id'
	 *				pos        === 'content position'
	 *				value 		 === Integer
	 *				callback -> returns to decrementContentPos
	 */
	$scope.updateManyWith = function(collection, shippingId, pos, value, callback) {
		DAOService.updateManyWith(collection, shippingId, pos, value, callback)
	}











	// Array Operations unused atm

	/* Purp.: Pushes an element to an array of a document of a given id
	 * 	where
	 *				shippingId === { id }
	 *        value      === { data: values }
	 *        callback -> returns values to 'databaseController'
	 */
	$scope.pushToArray = function(collection, shippingId, value, callback) {
		collection.update(shippingId, { $push: { value } }, function(err, res){
			callback(err, res)
		})
	}

	/* Purp.: Pulls an element from an array of a document of a given id
	 * 	where
	 *				shippingId === { id }
	 *				value      === { data: values }
	 *				callback -> returns values to 'databaseController'
	 */
	$scope.pullFromArray = function(collection, shippingId, value, callback) {
		collection.update(shippingId, { $pull: value }, function(err, res){
			callback(err, res)
		})
	}

});


/*
		//Update, create if not exists
		collection.update(
   				{ id_1: shipping.id },
   				{ $set: {data: shipping.data} },
   				{ upsert: true }
		);
*/