
/* This module represents the shipping that's been focused.
 * Once the cursor blurs from a shipping, the model is reseted to the focused shipping
 */

var model = angular.module('ShippingModelModule', []);

model.factory('ShippingModel', function(){

	var shippingModel = {}

	return {

		setIdModel: function(shippingId, currentValue, data, domElementId){
			shippingModel = {}
			shippingModel['id'] 				  = shippingId
			shippingModel['value']			  = currentValue
			shippingModel['domElemId']    = domElementId
			shippingModel['idComponents'] = { prefix: data.prefix, id: data.id }
		},

		setModel: function(shippingId, currentValue, domElementId){
			shippingModel = {}
			shippingModel['id'] 			 = shippingId
			shippingModel['value'] 		 = currentValue
			shippingModel['domElemId'] = domElementId
		},

		getModel: function(){
			return shippingModel
		},

		getDataId: function(){
			return shippingModel.idComponents
		},

		getPrefix: function(){
			return shippingModel['idComponents']['prefix']
		},

		getIdNumber: function(){
			return shippingModel['idComponents']['id']
		},

		getShippingId: function(){
			return shippingModel.id
		},

		getValue: function(){
			return shippingModel.value
		},

		getDomElementId: function(){
			return shippingModel.domElemId
		}

	}

})