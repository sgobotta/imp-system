
var model = angular.module('ShippingListModule', [])

model.$inject = ['SortingService']

model.factory('ShippingListModel', function(SortingService){


	var shippingList = []

  self.shippingsSort = {
													sortingOrder: '',
													reverse: false
	}	

	return {

  	getSortOrder: function(){
  		return self.shippingsSort.sortingOrder
  	},

  	getSortReverse: function(){
  		return self.shippingsSort.reverse
  	},

  	/* Uses the sorting service as a wrapper to get the list filtered before assigning
  	 */
  	setShippings: function(shippings){
  		shippingList = shippings
  	},

  	getShippings: function(){
  		return shippingList
  	},

  	addShipping: function(shipping){
  		console.log("length before push" + shippingList.length)
  		shippingList.push(shipping)
  		console.log("length after push" + shippingList.length)
  	},

  	removeShipping: function(shippingId){
  		console.log("length before filter" + shippingList.length)
  		shippingList = shippingList.filter(function(shipping){
  			return shipping.id !== shippingId
  		})
  		console.log("length after filter" + shippingList.length)
  	},

  	sortList: function(){
  		shippingList = SortingService.filter(shippingList, self.shippingsSort)
  	}

	}

})

model.$inject = ['$scope', 'ShippingListModel']

model.controller('ShippingListCtrl', function($scope, ShippingListModel){

	$scope.getShippings = function(){
		return ShippingListModel.getShippings()
	}

	$scope.shippingsSort = {
		sortingOrder: function() { return ShippingListModel.getSortOrder() },
		reverse: 			function() { return ShippingListModel.getSortReverse() }
	}	

})