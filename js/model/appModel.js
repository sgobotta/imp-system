
var model = angular.module("AppModelModule", [])

model.$inject = ['$scope', '$document', 'ContentModel']

model.factory('AppModel', function($document, ContentModel){

	var shippingRows = 0

	var lastBlurredElementId = 1

  var isInputFormVisible       = false;
  var isShippingDetailsVisible = true;
  var isContentFormVisible     = false;
  var isContentDetailsVisible  = false;

  return {

  	setShippingRows: function(rows){
  		shippingRows = rows
  	},

  	getShippingRows: function(){
  		return shippingRows
  	},

  	getLastBlurredElementId: function(){
  		return lastBlurredElementId
  	},

  	setLastBlurredElementId: function(elementId){
  		lastBlurredElementId = elementId
  	},

  	isInputFormVisible: function(){
  		return isInputFormVisible
  	},

  	isShippingDetailsVisible: function(){
  		return isShippingDetailsVisible
  	},

  	isContentFormVisible: function(){
  		return isContentFormVisible
  	},

  	isContentDetailsVisible: function(){
  		return isContentDetailsVisible
  	},

	  /* Toggles the shipping input html template
	   */
	  toggleInputForm: function(){
	    if(isInputFormVisible){
	    	console.log("Hiding Shipping Input Form")
	      isInputFormVisible = false;
	    } else if(!isContentFormVisible){
	    	console.log("Showing Shipping Input Form")
	      isInputFormVisible = true;
	      $document[0].getElementById("newShipping").focus()
	      // no da bola... seguramente porque primero quiere focusear antes de renderizar
	    }
	  },

		/* Shows the shipping content html template
	   */
	  showContentInputForm: function() {
	  	console.log("Showing Content Input Form")
	    isContentFormVisible = true;
	    ContentModel.setVisibility(true)
	    isContentDetailsVisible = true;
	    isShippingDetailsVisible = false;
	  },

	  /* Hides the shipping content html template
	   */
	  hideContentInputForm: function() {
	  	console.log("Hiding Content Input Form")
	    isContentFormVisible = false;
	    ContentModel.setVisibility(false)
	    isContentDetailsVisible = false;
	    isShippingDetailsVisible = true;
	  } 	  

  }

})

model.$inject = ['AppModel']

model.controller('AppModelCtrl', function($scope, AppModel, ContentModel){

	$scope.isInputFormVisible = function() {
		return AppModel.isInputFormVisible()
	}

	$scope.isContentFormVisible = function(){
		return AppModel.isContentFormVisible()
	}

	$scope.isShippingDetailsVisible = function(){
		return AppModel.isShippingDetailsVisible()
	}

	$scope.isContentDetailsVisible = function(){
		return AppModel.isContentDetailsVisible()
	}

	$scope.toggleInputForm = function(){
		AppModel.toggleInputForm()
	}

	$scope.showContentInputForm = function(){
		AppModel.showContentInputForm()
	}

	$scope.getContentInformation = function(property){
		if(AppModel.isContentDetailsVisible()){
			return ContentModel.getShippingInfo(property)
		}
	}

})