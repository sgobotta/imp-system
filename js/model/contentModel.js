
var model = angular.module('ContentModelModule', [])

model.factory('ContentModel', function() {

	var contentModel = {}
	var isVisible    = false

	return {

		setVisibility: function(boolean){
			isVisible = boolean
		},

		isVisible: function(){
			return isVisible
		},

		setModel: function(shippingId, index, currentValue, domElementId){
			contentModel['id'] 				= shippingId
			contentModel['pos'] 			= index
			contentModel['value'] 		= currentValue
			contentModel['domElemId'] = domElementId
		},

		setContent: function(content){
			contentModel['content'] = content
			contentModel['length']  = content.length
		},

		setShippingInfo: function(shipping){
			contentModel['id']						= shipping.prefix+shipping.id
			contentModel['consignee'] 	  = shipping.consignee
			contentModel['shipper'] 			= shipping.shipper
			contentModel['currency']  	  = shipping.currency
			contentModel['currencyValue'] = shipping.currencyValue
			contentModel['fobAmount'] 		= shipping.fobAmount
			contentModel['fob']						= shipping.fob
		},

		getShippingInfo: function(property){
			return contentModel[property]
		},

		getContent: function(){
			return contentModel.content
		},

		getLength: function(){
			return contentModel.length
		},

		getModel: function(){
			return contentModel
		},

		getId: function(){
			return contentModel.id
		},

		getIndex: function(){
			return contentModel.pos
		},

		getValue: function(){
			return contentModel.value
		},

		getDomElementId: function(){
			return contentModel.domElemId
		},

		getCurrency: function(){
			return contentModel.currency
		},

		getCurrencyValue: function(){
			return contentModel.currencyValue
		},

		getFob: function(){
			return contentModel.fobAmount
		},

		getRemainingFob: function(){
			return (contentModel.fobAmount - contentModel.content.reduce(function(a,b){
  			return a + parseFloat(b.data.contentFob)
  		}, 0)).toFixed(2)
		},

		refreshModel: function(){
			contentModel = {}
		}

	}

})

model.$inject = ['$scope', 'ContentModel']

model.controller('ContentModelCtrl', function($scope, ContentModel){

  /* Purp.: Returns the sum of a shipping content property
   *  where 
   *        content   === [ { shipping content } ]
   *        property  === 'content attribute'
   */
  $scope.getContentSum = function(property) {
    if(ContentModel.isVisible()) {
      return ContentModel.getContent().reduce(function(a,b){
        return a + parseFloat(b.data[property]);
      }, 0);
    }
  }

  $scope.getContent = function(){
  	if(ContentModel.isVisible()){
  		return ContentModel.getContent()	
  	}
  }

  $scope.getContentFob = function(contentFob){
  	if(ContentModel.isVisible()){
  		return (contentFob * ContentModel.getCurrencyValue()).toFixed(2)
  	}
  }

  $scope.getRemainingFob = function(){
  	if(ContentModel.isVisible()){
  		return ContentModel.getRemainingFob()
  	}
  }


})