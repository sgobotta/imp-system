
var model = angular.module('ParametersModule', [])

model.$inject = ['DBService']

model.factory('ParameterService', function(DBService, ValidationService){

	let shipping = {}
	let content  = {}

	function retrieveCurrencies(){
		DBService.getAllCurrencies(function(res){
			console.log("SETTING UP Currencies")
			console.log(res)
			shipping['currencies'] = res
			ValidationService.setCurrencies(res)
		})
	}

	function retrievePrefixes(){
		DBService.getAllPrefixes(function(res){
			console.log("SETTING UP Prefixes")
			console.log(res)
			shipping['prefixes'] = res
			ValidationService.setPrefixes(res)
		})		
	}

	function retrieveMeasures(){
		DBService.getContentMeasures(function(res){
			console.log("SETTING UP Measures")
			console.log(res)
			content['measures'] = res
			ValidationService.setMeasures(res)
		})
	}


	return {

		refreshParameters: function(){
			retrieveCurrencies()
			retrievePrefixes()
			retrieveMeasures()
		},

		/* Prec.: Parameters must have been retrieved and initialized
		 */
		getShippingCurrencies: function(callback){
			callback(shipping.currencies)
		},

		/* Prec.: Parameters must have been retrieved and initialized
		 */
		getShippingPrefixes: function(callback){
			callback(shipping.prefixes)
		},

		/* Prec.: Parameters must have been retrieved and initialized
		 */
		getContentMeasures: function(callback){
			callback(content.measures)
		}

	}




})