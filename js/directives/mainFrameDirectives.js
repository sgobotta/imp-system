var app = angular.module('MainFrameDirectives', []);


app.directive("shippingInput", function() {
	return {
 		restrict: 'A',
  		templateUrl: 'views/input-form.html'
	}
});

app.directive("shippingList", function() {
	return {
		restrict: 'A',
		templateUrl: 'views/shipping-list.html'
	}
})

app.directive("shippingTitle", function() {
	return {
		restrict: 'A',
		templateUrl: 'views/shipping-title.html'
	}
})

app.directive("shippingFooter", function() {
	return {
		restrict: 'A',
		templateUrl: 'views/shipping-footer.html'
	}
})

app.directive("shippingContent", function() {
	return {
		restrict: 'A',
		templateUrl: 'views/shipping-content.html'
	}
})