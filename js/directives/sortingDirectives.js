var sorting = angular.module('SortingDirectives', []);

// Sorting directive on
//  : template (could possibly be url)
//  : link
//  = function
sorting.directive("shippingsSort", function() {
return {
    restrict: 'A',
    transclude: true,    
    scope: {
      order: '=',
      sort: '='
    },
    template : 
      ' <a ng-click="sort_by(order)" style="color: #555555;">' +
      '    <span ng-transclude></span>' +
      '    <i ng-class="selectedCls(order)"></i>' +
      '</a>',
    link: function(scope) {
                
      // Evaluates and assigns a sorting state
      scope.sort_by = function(newSortingOrder) {       
        var sort = scope.sort;

        // if 'this' was clicked
        if (sort.sortingOrder === newSortingOrder){
          sort.reverse = !sort.reverse;
        }
        // assigns the given order no matter what
        sort.sortingOrder = newSortingOrder;        
    };
    
    // Evaluates and assigns a sorting icon
    scope.selectedCls = function(column) {
      if(column === scope.sort.sortingOrder){
        return ('icon-chevron-' + ((scope.sort.reverse) ? 'down' : 'up'));
      }
      else{            
        return'icon-sort' 
      } 
    };      
  }
}
});

sorting.directive("mainFrame", function() {
return {
  restrict: 'A',
  templateUrl: 'views/main-frame.html'
}

});