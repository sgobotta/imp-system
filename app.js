'use strict';

var app = angular.module('ModuleShipping', ['ngRoute',
																						'SortingControllers',
																						'SortingDirectives',
																						'MainFrameDirectives',
																						'DBModule',
																						'InputModule',
																						'ValidationModule',
																						'FeedbackModule',
																						'NavigationModule',
																						'electangular',
																						'ParametersModule',
																						'AppModelModule',
																						'ShippingListModule',
																						'ShippingModelModule',
																						'ContentModelModule',
																						'EventModule'
																						/*'TextParserModule'*/
																						/*'ParsingModule'*/
																					 ]
);

const fs = require('fs');

app.$inject = ['$rootScope', '$scope', 'electron', '$animate']

app.$inject = ['AppModel', 'ShippingListModel', 'DBService', 'ParameterService', 'NavigationService']

/*app.$inject = ['TextParserService']*/

app.factory('MainService', function(){

	function renderShippingList(){
		DBService.getAllShippings(function(itemlist){
			/*$scope.setShippings(itemlist);*/
			ShippingListModel.setShippings(itemlist)
			NavigationService.initialize(itemlist.length)
			AppModel.setShippingRows(itemlist.length)
		})
	}



})

app.controller('MainCtrl', function($rootScope, $scope, electron, $animate,
	AppModel, ShippingListModel, DBService, ParameterService, NavigationService) {

	ParameterService.refreshParameters()

	$animate.enabled(true);


	/* Gets all shippings from databasse, updates Navigation state, updates the main model and refreshes the list
	 */
	$scope.initializeShippings = function(){
		DBService.getAllShippings(function(itemlist){
			ShippingListModel.setShippings(itemlist)
			NavigationService.initialize(itemlist.length)
			AppModel.setShippingRows(itemlist.length)
		})
		renderShippings()
	}

	function renderShippings() {
		ShippingListModel.sortList()
	}

	/* Reads from a resource file, import json objects to the database, and refreshed the shipping list.
	 */
	$scope.importDocuments = function(){
		var content = {};
		var result = fs.readFileSync('resources/shipping.json', 'utf8');
		console.log("archivito");
		content = JSON.parse(result);
		var list = content['information']
/*		list = list.map(function(shipping){
			var fob = shipping.data['fobAmount']
			var currency = shipping.data['currency']
			shipping.data['fob'] = $scope.getFob(currency, fob)
			return shipping;
		})*/
		DBService.importList(list, "shippingDocuments");
		this.renderShippings();
	}

	function importFromJson(){
		var content = {}
		var result = fs.readFileSync('resources/measures.json', 'utf8')
		console.log("Reading from measures.json")
		content = JSON.parse(result)
		var list = content['measures']
		DBService.importList(list, "contentMeasures")
	}

	/*importFromJson()*/

	/* Refreshes the shippings list when the application starts
	 */
	$scope.initializeShippings();

	/*TextParserService.logFile()*/


});

