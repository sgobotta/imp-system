var app = angular.module("app", ["xeditable"]);

app.run(function(editableOptions) {
  editableOptions.theme = 'bs3';
});

app.controller('CtrlInput', function($scope) {
  $scope.shipping = {
    id: null,
    packages: null,
    weight: null,
    value: null,
    client: null,
    shipper: null
  };
});