

const {app, BrowserWindow, globalShortcut} = require('electron')
// Module to control application life.
const path = require('path')
// Module to create native browser window.
const url = require('url')

const {ipcMain} = require('electron').ipcMain;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

function createLoginWindow() {
  // Create the browser window.
  win = new BrowserWindow({width: 400, height: 400})

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'login.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  win.webContents.openDevTools()

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })

  win.on('blur', () => {

    /*globalShortcut.unregisterAll()
    win.webContents.send('main-window-blur', "main window blurs");*/
/*
  ipcMain.on('main-window-blur', (event, msg) => {

    //handle incoming message here
    console.log(msg);

    //message can be an Object
    if (msg.username == 'dude') {
      console.log(msg.access_level);
    }

  });
*/

  })

/*  win.addEventListener('error', function(e){
    var error = e.error;
    console.log(error);
  })*/
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  
  createLoginWindow();

})

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createLoginWindow()
  }
})
